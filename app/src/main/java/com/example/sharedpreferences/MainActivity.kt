package com.example.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        sharedPreferences=getSharedPreferences("data", Context.MODE_PRIVATE)
    }

    fun save(view: View){
        val email=emailEditText.text.toString()
        val firstName=firstNameEditText.text.toString()
        val lastName=lastNameEditText.text.toString()
        val age=ageEditText.text.toString()
        val address=addressEditText.text.toString()
        if(address.isNotEmpty()&&age.isNotEmpty()&&lastName.isNotEmpty()&&firstName.isNotEmpty()&&email.isNotEmpty()){
            val editor=sharedPreferences.edit()
            val age1=age.toInt()
            editor.putString("firstName",firstName)
            editor.putString("lastName",lastName)
            editor.putInt("age",age1)
            editor.putString("email",email)
            editor.putString("address",address)
            editor.apply()

        }
        else{
            Toast.makeText(this,"please fill all fields",Toast.LENGTH_SHORT).show()
        }



    }
    fun read(view: View){
        val firstName=sharedPreferences.getString("firstName"," ")
        val lastName=sharedPreferences.getString("lastName"," ")
        val age:Int=sharedPreferences.getInt("age",0)
        val address =sharedPreferences.getString("address"," ")
        val email=sharedPreferences.getString("email"," ")

        firstNameEditText.setText(firstName)
        lastNameEditText.setText(lastName)
        ageEditText.setText(age.toString())
        addressEditText.setText(address)
        emailEditText.setText(email)


    }
    fun delete(view: View){
        val editor=sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }
}